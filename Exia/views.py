from django.shortcuts import render
from django.http import HttpResponse
import datetime

# Create your views here.
def index(request):
    return render(request,'index.html')

def readinglist(request):
    return render(request, 'readinglist.html')

def time_now(request):
    date_now=datetime.datetime.now().strftime("%H:%M:%S")
    html = 'time.html'
    return render(request,html,{'now': date_now})

def time(request,addition):
    addition=int(addition)
    date_now=datetime.datetime.now()
    hour=(datetime.datetime.now().hour+addition)%24
    date_now=date_now.replace(hour=hour).strftime("%H:%M:%S")
    html = 'time.html'
    return render(request,html,{'now': date_now})