from django.urls import path
from .views import *

urlpatterns = [
    path('',index,name='exia-index'),
    path('readinglist/',readinglist,name='exia-readlist'),
    path('time/',time_now),
    path('time/<str:addition>/',time),
]