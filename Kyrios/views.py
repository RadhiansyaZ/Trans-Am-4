from django.shortcuts import render,HttpResponseRedirect,redirect
from .forms import MatkulForm
from .models import MataKuliah,Tugas

import datetime

# Create your views here.
def create(request):
    submitted=False
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('?submitted=True')
    else:
        form = MatkulForm()
        if 'submitted' in request.GET:
            submitted=True
    return render(request,'create.html',{'form': form, 'submitted' : submitted})

def course_show(request):
    if request.method == "POST":
        MataKuliah.objects.get(id=request.POST['id']).delete()
        return redirect('/course/')
    matkul_db = MataKuliah.objects.all()
    passing_arg={
        'db':matkul_db, 
        'isThere':True
        }
    return render(request,'course_list.html',context=passing_arg)

def course_details(request, pass_id):
    passing_arg = {}
    data = MataKuliah.objects.get(pk=pass_id)
    assg = Tugas.objects.all().filter(matkul_id=data)
    overdue = assg.filter(deadline__lt=datetime.date.today())
    near = assg.filter(deadline__range=(datetime.date.today(), datetime.date.today() + datetime.timedelta(days = 7)))
    far = assg.filter(deadline__gt = datetime.date.today() + datetime.timedelta(days = 7))
    # print(data)
    # for e in assg:
    #     print(e.nama_tugas)   
    passing_arg = {
        'item': data,
        'overdue':overdue,
        'near':near,
        'far':far,
    }
    return render(request, 'course_details.html',context=passing_arg)