from django.db import models

class MataKuliah(models.Model):
    nama_matkul = models.CharField('Nama Mata Kuliah', max_length=80)
    dosen_pengajar = models.CharField('Dosen Pengajar', max_length=80)
    jumlah_sks = models.IntegerField('Jumlah SKS')
    deskripsi = models.TextField(blank=True)
    semester = models.CharField('Semester tahun', max_length=40)
    kelas = models.CharField('Ruang Kelas', max_length=40)
    
    def __str__(self):
        return self.nama_matkul

class Tugas(models.Model):
    nama_tugas = models.CharField('Nama Tugas', max_length=80)
    deadline = models.DateField()
    matkul = models.ForeignKey(MataKuliah, on_delete=models.CASCADE)

    class Meta():
        ordering=['deadline']

    def __str__(self):
        return self.nama_tugas