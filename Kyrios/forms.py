from django import forms
from django.forms import ModelForm
from .models import MataKuliah

#class MatkulForm(forms.Form):
#    nama_matkul = forms.CharField(label='Nama Mata Kuliah', max_length=50)
#    dosen_pengajar = forms.CharField(label='Dosen Pengajar', max_length=50)
#    jumlah_sks = forms.CharField(label='Jumlah SKS', max_length=15)
#    deskripsi = forms.CharField(label='Deskripsi mata kuliah')
#    semester = forms.CharField(label='Semester tahun', max_length=20)
#    kelas = forms.CharField(label='Ruang Kelas', max_length=30)

class MatkulForm(ModelForm):
    required_class='required'
    class Meta:
        model=MataKuliah
        fields = '__all__'