from django.apps import AppConfig


class KyriosConfig(AppConfig):
    name = 'Kyrios'
