from django.urls import path
from . import views

urlpatterns = [
    path('', views.course_show,name='Course'),
    path('create/',views.create,name='Course_add'),
    path('details/<int:pass_id>',views.course_details,name='detail')
]