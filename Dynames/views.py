from django.shortcuts import render,redirect,HttpResponseRedirect,get_object_or_404
from .models import Aktivitas,Peserta
from .forms import AktivitasForm,PesertaForm

# Create your views here.
def activity_show(request):
    # if request.method == "POST":
        # return redirect('/activity/')
    aktivitas_db = Aktivitas.objects.all()
    peserta_db = Peserta.objects.all()
    form = AktivitasForm()
    form_peserta = PesertaForm()
    if request.method== "POST":
        form = AktivitasForm(request.POST)
        if form.is_valid():
            form.save()
            form = AktivitasForm()
    else:
        AktivitasForm()
    passing_arg =  {
        'form':form,
        'db_ak':aktivitas_db,
        'db_pers':peserta_db,
        'isThere':True
    }
    return render(request, 'activity_list.html',context=passing_arg)

def add_guest(request, id):
    peserta_db={}
    kegiatan = get_object_or_404(Aktivitas, pk=id)
    if request.method == 'POST':
       form_peserta = PesertaForm(request.POST)
       if form_peserta.is_valid():
            #aktivitas_db = Aktivitas.objects.all()
            peserta_db = form_peserta.save(commit=False)
            peserta_db.kegiatan = kegiatan
            peserta_db.save()
            return redirect('/activity/')
    else:
        form_peserta = PesertaForm()
    duar = {
        'peserta':peserta_db,
        'form':form_peserta
    }
    return render(request, 'create_guest.html', context=duar)
