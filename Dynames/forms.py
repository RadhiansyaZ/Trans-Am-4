from django import forms
from .models import Aktivitas,Peserta

class AktivitasForm(forms.ModelForm):
    aktivitas = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto'
        }, 
    ))
    class Meta:
        model = Aktivitas
        fields = (
            'aktivitas',
            )
class PesertaForm(forms.ModelForm):
    nama = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto'
        }, 
    ))
    class Meta:
        model = Peserta
        fields = (
            'nama',
        )