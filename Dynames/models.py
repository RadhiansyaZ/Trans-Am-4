from django.db import models

# Create your models here.
class Aktivitas(models.Model):
    aktivitas = models.CharField(max_length=30)

    # def __str__(self):
        # return self.aktivitas

class Peserta(models.Model):
    nama = models.CharField(max_length=30)
    kegiatan = models.ForeignKey(Aktivitas, on_delete=models.CASCADE)

    # def __str__(self):
    #     return self.nama