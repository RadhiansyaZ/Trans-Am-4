from django.test import TestCase,Client
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from .views import activity_show,add_guest
from .models import Aktivitas,Peserta
from .forms import AktivitasForm,PesertaForm

# Create your tests here.
class TestDynames(TestCase):
    def setUp(self):
        self.client = Client()
        self.aktivitas = Aktivitas.objects.create(aktivitas = "sebat")
        self.peserta = Peserta.objects.create(nama = "opal", kegiatan = self.aktivitas)

    #Exist
    def test_activity_page_is_exist(self):
        response = self.client.get('/activity/')
        self.assertEqual(response.status_code, 200)

    def test_activity_page_post_success_and_render_the_result(self):
        response_post = self.client.post(
            '/activity/', {
                'nama_event': 'Mabok_bareng'
            }
        )
        self.assertEqual(response_post.status_code, 200)

    def test_activity_add_guest_page_post_not_exist_and_render_the_result(self):
        response_post = self.client.post(
            '/event/add/sabar', {
                'nama': 'alghi'
            }
        )
        self.assertEqual(response_post.status_code, 404)

    def test_add_intan_404_render_result(self):
        response_post = self.client.post(
            '/event/add/dsadsa', {
                'nama': 'intan'
            }
        )
        self.assertEqual(response_post.status_code, 404)

    def test_add_putsal_404_render_result(self):
        response_post = self.client.post(
            '/event/add/dsa', {
                'nama': 'putsal'
            }
        )
        self.assertEqual(response_post.status_code, 404)
    
    def test_add_kevin_404_render_result(self):
        response_post = self.client.post(
            '/event/add/picdsa', {
                'nama': 'kevin'
            }
        )
        self.assertEqual(response_post.status_code, 404)

    def test_add_azis_404_render_result(self):
        response_post = self.client.post(
            '/event/add/kastrat', {
                'nama': 'azis'
            }
        )
        self.assertEqual(response_post.status_code, 404)

    def test_add_nabil_tegar_404_render_result(self):
        response_post = self.client.post(
            '/event/add/bigojek', {
                'nama': 'nabil'
            }
        )
        self.assertEqual(response_post.status_code, 404)


    #Views
    def test_activity_page_using_view_func(self):
        found = resolve('/activity/')
        self.assertEqual(found.func, activity_show)
    
    #Models
    def test_page_uses_index_template_after_post(self):
        new_act = Aktivitas.objects.create(aktivitas="jalan-jalan",id=3)
        new_act.save()
        response = self.client.post('/activity/',{'idnya':3,'anggota-baru':"ajay"})
        self.assertEqual(response.status_code, 200)

    def test_page_add_guests(self):
        new_act = Aktivitas.objects.create(aktivitas="fahdii",id=4)
        new_act.save()
        response = self.client.post('/activity/create/4',{'idnya':4,'anggota-baru':"ajay"})
        self.assertEqual(response.status_code, 200)

    #Templates
    def test_page_uses_index_template(self):
        response = Client().get('/activity/')
        self.assertTemplateUsed(response, 'activity_list.html')
    
    #Forms
    def test_Aktivitas_forms(self):
        form = AktivitasForm()
        form_data = {'aktivitas': 'mabok'}
        form = AktivitasForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_Peserta_forms(self):
        form = PesertaForm()
        form_data = {'nama': 'andi','kegiatan':'tidur'}
        form = PesertaForm(data=form_data)
        self.assertTrue(form.is_valid())
