from django.urls import path
from . import views

urlpatterns = [
    path('', views.activity_show,name='Activity'),
    path('create/<int:id>',views.add_guest,name='add_guest'),
]