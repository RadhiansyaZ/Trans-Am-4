# Generated by Django 3.0.2 on 2020-03-06 05:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Aktivitas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('aktivitas', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Peserta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=30)),
                ('kegiatan', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Dynames.Aktivitas')),
            ],
        ),
    ]
