from django.apps import AppConfig


class AstraeaConfig(AppConfig):
    name = 'Astraea'
